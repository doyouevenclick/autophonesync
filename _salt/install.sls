adb:
  pkg.installed

android-sdk-platform-tools-common:  # Recommends of adb
  pkg.installed

autophonesync:
  pip.installed:
    - bin_env: /usr/bin/pip3

autophonesync install:
  cmd.run:
    - creates: /etc/udev/rules.d/99-autophonesync.rules
    - requires:
      - pip: autophonesync
    - reset_system_locale: false
