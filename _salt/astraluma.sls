include:
- install

/home/astraluma/suraya:
  file.directory:
    - user: astraluma

/home/astraluma/.config:
  file.directory:
    - user: astraluma

/home/astraluma/.config/phonesync:
  file.managed:
    - user: astraluma
    - contents: |
        [801KPXV1506706]
        destination = /home/astraluma/suraya
        prescript =
        postscript =
